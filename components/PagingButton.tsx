export const PagingButton = ({
  text,
  onClick,
  active,
}: {
  text: string | number
  onClick: () => void
  active?: boolean
}) => {
  return (
    <button
      type="button"
      className={`text-white bg-gray-800 hover:bg-gray-700 focus:outline-none focus:ring-4 focus:ring-gray-300 font-medium rounded-lg text-sm px-3 py-2 dark:focus:ring-gray-700 dark:border-gray-700 ${
        active ? 'ring-gray-300' : ''
      }`}
      onClick={onClick}
    >
      {text}
    </button>
  )
}

export default PagingButton
