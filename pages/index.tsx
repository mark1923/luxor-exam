import type { NextPage, NextPageContext } from 'next'
import Head from 'next/head'
import { useCallback, useState } from 'react'
import { v4 as uuidv4 } from 'uuid'
import { useRouter } from 'next/router'
import nookies, { setCookie } from 'nookies'
import styles from '../styles/Home.module.css'

export async function getServerSideProps(ctx: NextPageContext) {
  const cookies = nookies.get(ctx)

  if (cookies?.session) {
    return {
      redirect: {
        destination: '/pokemons',
        permanent: false,
      },
    }
  }

  return { props: {} }
}

const Home: NextPage = () => {
  const router = useRouter()
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const [error, setError] = useState(false)
  const [welcome, setWelcome] = useState(false)

  const onLogin = useCallback(() => {
    if (
      username === process.env.USERNAME &&
      password === process.env.PASSWORD
    ) {
      setError(false)
      setWelcome(true)
      setCookie(null, 'session', uuidv4(), {
        maxAge: 30 * 24 * 60 * 60,
        path: '/',
      })
      router.push('/pokemons')
    } else {
      setError(true)
      setWelcome(false)
    }
  }, [username, password])

  return (
    <div className={styles.container}>
      <Head>
        <title>Luxor Exam by MARK ANTHONY UY</title>
        <meta name="description" content="Luxor Exam by MARK ANTHONY UY" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        {welcome && (
          <div
            className="p-4 mb-4 text-sm text-green-700 bg-green-100 rounded-lg dark:bg-green-200 dark:text-green-800"
            role="alert"
          >
            <span className="font-medium">Success: </span> Welcome Admin!
            Redirecting...
          </div>
        )}
        {error && (
          <div
            className="p-4 mb-4 text-sm text-red-700 bg-red-100 rounded-lg dark:bg-red-200 dark:text-red-800"
            role="alert"
          >
            <span className="font-bold">Error: </span>Check your credentials and
            try again.
          </div>
        )}
        <div className="min-h-full flex items-center justify-center py-12 px-4 sm:px-6 lg:px-8  bg-neutral-800">
          <div className="max-w-md w-full py-5 px-4">
            <form className="space-y-6" action="#" method="POST">
              <div className="rounded-md shadow-sm">
                <div className="mb-5">
                  <label className="sr-only">Email address</label>
                  <input
                    id="username"
                    name="username"
                    required
                    className="bg-neutral-600 relative block w-full px-3 py-2 focus:border border-neutral-300 placeholder-gray-300 text-gray-900 rounded-md focus:outline-none focus:ring-yellow-300 focus:border-yellow-300 focus:z-10 sm:text-sm text-stone-200"
                    placeholder="username"
                    onChange={(e) => setUsername(e.currentTarget.value)}
                  />
                </div>
                <div className="mb-5">
                  <label className="sr-only">Password</label>
                  <input
                    id="password"
                    name="password"
                    type="password"
                    required
                    className="bg-neutral-600 relative block w-full px-3 py-2 focus:border border-neutral-300 placeholder-gray-300 text-gray-900 rounded-md focus:outline-none focus:ring-yellow-300 focus:border-yellow-300 focus:z-10 sm:text-sm text-stone-200"
                    placeholder="Password"
                    onChange={(e) => setPassword(e.currentTarget.value)}
                  />
                </div>
              </div>

              <div>
                <button
                  type="button"
                  className="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-yellow-400 hover:bg-yellow-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                  onClick={onLogin}
                >
                  Login
                </button>
              </div>
            </form>
          </div>
        </div>
        <p className="text-neutral-600 text-xs mt-10 text-center">
          Created by: Mark Uy
        </p>
      </main>
    </div>
  )
}

export default Home
