export interface PokemonListItem {
  id: string
  name: string
  image: string
  number: string
  __typename: string
}

interface PokemonDimension {
  minimum: string
  maximum: string
}

interface Attack {
  name: String
  type: String
  damage: number
}
interface PokemonAttack {
  fast: Attack
  special: Attack
}

interface PokemonEvolutionRequirement {
  amount: number
  name: string
}

export interface Pokemon {
  id: string
  number: string
  name: string
  weight: PokemonDimension
  height: PokemonDimension
  classification: string
  types: string[]
  resistant: string[]
  attacks: PokemonAttack
  weaknesses: string[]
  fleeRate: number
  maxCP: number
  evolutions: Pokemon[]
  evolutionRequirements: PokemonEvolutionRequirement
  maxHP: number
  image: string
}
