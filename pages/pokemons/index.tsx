import { useQuery } from '@apollo/client'
import client from '../../apollo-client'
import { PokemonListItem, Pokemon } from '../../types/Pokemons'
import { useCallback, useState } from 'react'
import { Spinner, SidebarItem, Box, PagingButton } from '../../components'
import nookies, { destroyCookie } from 'nookies'
import { NextPageContext } from 'next'
import { getPokemon, getAllPokemon } from '../../queries/pokemon'
import { PER_PAGE, TOTAL } from '../../constants'
import { useRouter } from 'next/router'

export async function getServerSideProps(ctx: NextPageContext) {
  const cookies = nookies.get(ctx)

  if (!cookies?.session) {
    return {
      redirect: {
        destination: '/',
        permanent: false,
      },
    }
  }

  const { data } = await client.query({
    query: getAllPokemon,
  })

  return {
    props: {
      pokemons: data.pokemons,
    },
  }
}

const Pokemons = ({ pokemons }: { pokemons: PokemonListItem[] }) => {
  const [paginatedPokemons, setPaginatedPokemons] = useState(
    pokemons.slice(0, PER_PAGE)
  )
  const [selectedPokemon, setSelectedPokemon] = useState('')
  const { data, loading, error } = useQuery<{
    pokemon: Pokemon
  } | null>(getPokemon, {
    variables: {
      name: selectedPokemon,
    },
  })
  const [currentPage, setCurrentPage] = useState(1)
  const router = useRouter()

  const onPageClick = useCallback(
    (page: number, i: number) => {
      setCurrentPage(page)
      setPaginatedPokemons([
        ...pokemons.slice(
          (i === 0 ? 0 : i) * PER_PAGE,
          (i === 0 ? 1 : i) * PER_PAGE + (i === 0 ? 0 : PER_PAGE)
        ),
      ])
    },
    [pokemons]
  )

  const onPrevClick = useCallback(() => {
    if (currentPage <= 1) {
      return
    }

    const page = currentPage - 1
    setCurrentPage(page)

    setPaginatedPokemons([
      ...pokemons.slice(
        (page === 0 ? 0 : page - 1) * PER_PAGE,
        (page === 0 ? 1 : page - 1) * PER_PAGE + (page === 0 ? 0 : PER_PAGE)
      ),
    ])
  }, [currentPage, pokemons])

  const onNextClick = useCallback(() => {
    if (currentPage >= 4) {
      return
    }

    const page = currentPage + 1
    setCurrentPage(page)

    setPaginatedPokemons([
      ...pokemons.slice(
        (currentPage === 0 ? 0 : currentPage) * PER_PAGE,
        (currentPage === 0 ? 1 : currentPage) * PER_PAGE +
          (currentPage === 0 ? 0 : PER_PAGE)
      ),
    ])
  }, [currentPage, pokemons])

  const onLogout = () => {
    destroyCookie(null, 'session')
    router.push('/')
  }

  return (
    <main className="flex min-h-screen h-48 md:h-full flex-wrap">
      <aside className="w-full md:w-2/5 md:min-h-screen md:h-full h-1/2 relative overflow-hidden">
        <div className="overflow-y-auto py-6 px-6 bg-gray-500 rounded dark:bg-gray-800 h-screen">
          <ul className="space-y-2 mb-10">
            {paginatedPokemons.map(({ name, number, image }) => (
              <SidebarItem
                key={name}
                name={name}
                number={number}
                image={image}
                selected={selectedPokemon}
                onPress={setSelectedPokemon}
              />
            ))}
          </ul>
        </div>
        <div className="absolute bottom-0 left-0 bg-gray-900 w-full flex flex-s p-3 justify-between">
          <div className="space-x-2">
            {[...Array(Math.ceil(TOTAL / PER_PAGE))].map((_, i) => {
              const page = i + 1
              return (
                <PagingButton
                  key={`page-${page}`}
                  active={currentPage == page}
                  text={page}
                  onClick={() => onPageClick(page, i)}
                />
              )
            })}
          </div>
          <div className="space-x-2">
            <PagingButton text="Prev" onClick={onPrevClick} />
            <PagingButton text="Next" onClick={onNextClick} />
          </div>
        </div>
      </aside>
      <div className="w-full md:w-3/5 md:h-screen relative bg-gray-600 overflow-y-auto pb-10 min-h-screen">
        <div className="flex justify-center py-2">
          <button
            type="button"
            className="text-white bg-gray-800 hover:bg-gray-700 focus:outline-none focus:ring-4 focus:ring-gray-300 font-medium rounded-lg text-sm px-3 py-2 dark:focus:ring-gray-700 dark:border-gray-700"
            onClick={onLogout}
          >
            Logout
          </button>
        </div>
        {loading ? (
          <Spinner />
        ) : data ? (
          <div className="">
            <h1 className="flex justify-between space-x-2 mb-5 p-5 border-b border-gray-900">
              <span className="text-4xl text-gray-100">
                {data?.pokemon?.name}
              </span>
              <span className="text-yellow-400 text-xl italic">
                #{data?.pokemon?.number}
              </span>
            </h1>

            <figure className="flex justify-center mb-5">
              <img
                className="w-22 h-22 rounded-full"
                src={data?.pokemon?.image}
              />
            </figure>

            <div className="grid grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4 px-4">
              <Box
                title="Weight"
                value={`${data.pokemon.weight.minimum} - ${data.pokemon.weight.maximum}`}
              />
              <Box
                title="Height"
                value={`${data.pokemon.height.minimum} - ${data.pokemon.height.maximum}`}
              />
              <Box title="Classification" value={data.pokemon.classification} />
              <Box title="Flee Rate" value={data.pokemon.fleeRate} />
              <Box title="Max HP" value={data.pokemon.maxHP} />
              <Box title="Max CP" value={data.pokemon.maxCP} />
              <Box
                title="Types"
                value={data.pokemon.types.toString().replaceAll(',', ', ')}
              />
              <Box
                title="Weakness"
                value={data.pokemon.weaknesses.toString().replaceAll(',', ', ')}
              />
              <Box
                title="Resistant"
                value={data.pokemon.resistant.toString().replaceAll(',', ', ')}
              />
            </div>
          </div>
        ) : null}
      </div>
    </main>
  )
}

export default Pokemons
