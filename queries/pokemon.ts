import { gql } from '@apollo/client'
import { TOTAL } from '../constants'

export const getAllPokemon = gql`
query Pokemons {
  pokemons(first: ${TOTAL}) {
    id
    name
    number
    image
  }
}
`

export const getPokemon = gql`
  query Pokemon($name: String) {
    pokemon(name: $name) {
      id
      name
      number
      image
      weight {
        minimum
        maximum
      }
      height {
        minimum
        maximum
      }
      classification
      types
      resistant
      attacks {
        fast {
          name
        }
        special {
          name
        }
      }
      weaknesses
      fleeRate
      maxCP
      maxHP
    }
  }
`
