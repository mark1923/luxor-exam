export const Box = ({
  title,
  value,
}: {
  title: string
  value: string | number
}) => (
  <div className="bg-gray-800 p-3 rounded-md">
    <h3 className="text-gray-400 text-xs mb-1">{title}</h3>
    <p className="text-yellow-300 text-sm">{value}</p>
  </div>
)

export default Box
