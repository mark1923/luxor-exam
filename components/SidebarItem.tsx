export const SidebarItem = ({
  name,
  number,
  image,
  onPress,
  selected,
}: {
  name: string
  number: string
  image: string
  onPress: (name: string) => void
  selected: string
}) => (
  <li key={name}>
    <button
      className={`w-full flex items-center p-2 text-base font-normal text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700 flex items-center space-x-4 ${
        selected === name ? 'bg-gray-700 pointer-events-none' : 'bg-gray-600'
      }`}
      type="button"
      onClick={() => onPress(name)}
    >
      <img className="w-10 h-10 rounded-full shadow-lg" src={image} />
      <span className="text-yellow-300">{number}</span>
      <span className="">{name}</span>
    </button>
  </li>
)

export default SidebarItem
